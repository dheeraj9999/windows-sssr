
#Create VPC for Windows 2012R2 SQL
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  tags = {
    Name = "windows2012R2_SQL_vpc"
  }
}

#Define the  public subnet
resource "aws_subnet" "public-subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnet_cidr
  availability_zone       = var.aws_az
  map_public_ip_on_launch = true
  tags = {
    Name = "windows2012R2_SQL-public-subnet"
  }
}

#Define the internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "windows2012R2_SQL-igw"
  }
}

#Define the  public route table
resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "windows2012R2_SQL_public-subnet-rt"
  }
}

#Assign the public route table to the public Subnet
resource "aws_route_table_association" "public-rt-association" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.public-rt.id
}


# module "vpc" {
#   source = git::https://gitlab.ztri3.support/infrastructure/modules/iceberg-vpc.git
#   env    = var.environment
# }
