data "template_file" "user_data_init" {
  template = file("${path.module}/scripts/adduser.tpl")
  vars = {
    ssrs_username = var.ssrs_username
    ssrs_password = var.ssrs_password
  }
}

# resource "local_file" "user_data" {
#   content  = data.template_file.user_data_init.rendered
#   filename = "user_data-${sha1(data.template_file.user_data_init.rendered)}.ps1"
# }

##############################################
# Get latest Windows 2012 R2  
data "aws_ami" "windows2012" {
  most_recent = true
  owners      = ["801119661308"] # Canonical
  filter {
    name   = "name"
    values = ["Windows_Server-2012-R2_RTM-English-64Bit-SQL_2014_SP3_Standard-2021.11.10"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}


output "windows_ami_id" {
  value = data.aws_ami.windows2012.id
}