variable "aws_region" {
  default = "us-east-1"
}

variable "ssrs_username" {
  type = string
}

variable "ssrs_password" {
  type = string
}

variable "instance_type" {
  type        = string
  description = "EC2 instance type for SQL Server"
}

variable "aws_az" {
  type        = string
  description = "AZ of SQL EBS  Storage"
}



# variable "vpc" {
#   description = "Id of the VPC"
#   type        = string
#   default     = "vpc-d361ccb6"
# }
variable "vpc_cidr" {
  description = "CIDR for the Dev VPC"
  default     = "10.0.0.0/16"
}
variable "public_subnet_cidr" {
  description = "CIDR for the Dev public subnet"
  default     = "10.0.10.0/28"
}
