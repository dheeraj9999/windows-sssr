#Create a resource for ec2 instance
resource "aws_instance" "windows2012r2sql" {
  ami           = data.aws_ami.windows2012.id
  instance_type = var.instance_type
  key_name      = "windows-keypair-ssrs"
  subnet_id     = aws_subnet.public-subnet.id
  #subnet_id              = module.vpc.aws_subnet.private_east1a.id
  vpc_security_group_ids = [aws_security_group.windows2012R2-sg.id]
  iam_instance_profile   = aws_iam_instance_profile.admin_ssm_win_profile.id
  user_data              = data.template_file.user_data_init.rendered

  # root disk
  root_block_device {
    volume_size           = 200
    delete_on_termination = true
  }


  tags = {
    Name       = "Windows_Server_2012R2_SQL"
    Managed-by = "terraform"
  }

}
#EBS Volume and Attachment

resource "aws_ebs_volume" "SQLR01_Ddrive" {
  availability_zone = var.aws_az
  size              = 1024
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.SQLR01_Ddrive.id
  instance_id = aws_instance.windows2012r2sql.id
}
resource "aws_ebs_volume" "SQLR01_Backup_Edrive" {
  availability_zone = var.aws_az
  size              = 350
}

resource "aws_volume_attachment" "ebs1_att" {
  device_name = "/dev/sdt"
  volume_id   = aws_ebs_volume.SQLR01_Backup_Edrive.id
  instance_id = aws_instance.windows2012r2sql.id
}

resource "aws_ebs_volume" "ReportRep_Backup_Fdrive" {
  availability_zone = var.aws_az
  size              = 350
}
resource "aws_volume_attachment" "ebs2_att" {
  device_name = "/dev/sdf"
  volume_id   = aws_ebs_volume.ReportRep_Backup_Fdrive.id
  instance_id = aws_instance.windows2012r2sql.id
}