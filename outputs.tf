output "id" {
  value = aws_instance.windows2012r2sql.id
}

output "public_ip" {
  value = aws_instance.windows2012r2sql.public_ip
}

output "key_pair_name" {
  value = module.Key_pair.key_pair_key_name
}
