#key pair 

resource "tls_private_key" "this" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

module "Key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name   = "windows-keypair-ssrs"
  public_key = tls_private_key.this.public_key_openssh
}

resource "local_file" "pem_key" {
  filename = "windows-ssrs.pem"
  content  = tls_private_key.this.private_key_pem
}

#Instance Role
resource "aws_iam_role" "ssm_win_admin_role" {
  name               = "admin-ssm-win-ec2"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name    = "admin-ssm-win-ec2"
    Owner   = "DevOps"
    Project = "Windows SSRS"
  }
}

#Instance Profile
resource "aws_iam_instance_profile" "admin_ssm_win_profile" {
  name = "admin-ssm-win-ec2"
  role = aws_iam_role.ssm_win_admin_role.id
}

#Attach Policies to Instance Role
resource "aws_iam_policy_attachment" "admin_ssm_win_attach1" {
  name       = "admin-ssm-win-attachment"
  roles      = [aws_iam_role.ssm_win_admin_role.id]
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

